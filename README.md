# Secret Box

This package lightly wraps around `tweetnacl.js` and `tweetnacl-util` to make it simpler to encrypt and decrypt strings with a given secret key. It's basically this example as a package: https://github.com/dchest/tweetnacl-js/wiki/Examples