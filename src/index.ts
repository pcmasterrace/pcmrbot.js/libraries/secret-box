import { secretbox as secretBox, randomBytes } from "tweetnacl";
import { decodeBase64, encodeBase64, decodeUTF8, encodeUTF8 } from "tweetnacl-util";

/**
 * Generates a random 24-byte long nonce.
 */
export function generateNonce(): Uint8Array {
    return randomBytes(secretBox.nonceLength)
}

/** 
 * Generates a random 32-byte long secret key, encoded in Base64.
 */
export function generateKey(): string {
    return encodeBase64(randomBytes(secretBox.keyLength));
}

/**
 * Encrypts a raw string using the specified key. 
 * 
 * @param {string} raw - The string to encode. 
 * @param {string} key - A Base64-encoded secret key.
 * @returns {string} The encrypted version of the `raw` string
 */
export function encrypt(raw: string, key: string): string {
    const keyUint8Array = decodeBase64(key);

    const nonce = generateNonce();
    const messageUint8 = decodeUTF8(raw);
    const box = secretBox(messageUint8, nonce, keyUint8Array);

    const fullMessage = new Uint8Array(nonce.length + box.length);
    fullMessage.set(nonce);
    fullMessage.set(box, nonce.length);

    return encodeBase64(fullMessage);
}

/**
 * Decrypts an encrypted string using the specified key.
 * 
 * @param {string} messageWithNonce - The Base64-encoded encrypted message, with the first 24 bytes containing the nonce. 
 * @param {string} key - A Base64-encoded secret key.
 * @returns {string} The decrypted string.
 * @throws Throws if the decryption fails. 
 */
export function decrypt(messageWithNonce: string, key: string): string {
    const keyUint8Array = decodeBase64(key);
    const messageWithNonceUint8 = decodeBase64(messageWithNonce);
    const nonce = messageWithNonceUint8.slice(0, secretBox.nonceLength);
    const message = messageWithNonceUint8.slice(secretBox.nonceLength, messageWithNonce.length);

    const decrypted = secretBox.open(message, nonce, keyUint8Array);
    if (!decrypted) {
        throw new Error("Could not decrypt message");
    }

    return encodeUTF8(decrypted);
}

/**
 * This class allows for easy encoding and decoding of information using the same secret key.
 */
export class Cipher {
    private _key: string;

    /**
     * Creates a new Cipher. 
     * 
     * @param {string} [secretKey] - A Base64-encoded 32-byte long secret key. Will generate a new key if none is provided. 
     */
    constructor(secretKey: string = generateKey()) {
        this._key = secretKey;
    }

    /**
     * Encrypts a message.
     * @param {string} message - The message to be encrypted. 
     * @returns {string} The encrypted message.
     */
    encrypt(message: string): string {
        return encrypt(message, this._key);
    }

    /** 
     * Decrypts a message.
     * 
     * @param {string} message - The Base64-encoded encrypted message to be decrypted.
     * @returns {string} The decrypted message.
     * @throws Throws an error if the decryption fails.
     */
    decrypt(message: string): string {
        return decrypt(message, this._key);
    }

    /**
     * @returns {string} The cipher's secret key
     */
    get key(): string {
        // I should probably figure out why I have this getter in here at all
        return this._key;
    }
    
    /**
     * Overrides toJSON to redact secret key when stringifying
     */
    toJSON() {
        let result = {};
        for (let x in this) {
            if (x !== "_key") {
                result[x] = this[x];
            } else {
                result[x] = "[REDACTED]"
            }
        }
        return result;
    }
}
