/**
 * Generates a random 24-byte long nonce.
 */
export declare function generateNonce(): Uint8Array;
/**
 * Generates a random 32-byte long secret key, encoded in Base64.
 */
export declare function generateKey(): string;
/**
 * Encrypts a raw string using the specified key.
 *
 * @param {string} raw - The string to encode.
 * @param {string} key - A Base64-encoded secret key.
 * @returns {string} The encrypted version of the `raw` string
 */
export declare function encrypt(raw: string, key: string): string;
/**
 * Decrypts an encrypted string using the specified key.
 *
 * @param {string} messageWithNonce - The Base64-encoded encrypted message, with the first 24 bytes containing the nonce.
 * @param {string} key - A Base64-encoded secret key.
 * @returns {string} The decrypted string.
 * @throws Throws if the decryption fails.
 */
export declare function decrypt(messageWithNonce: string, key: string): string;
/**
 * This class allows for easy encoding and decoding of information using the same secret key.
 */
export declare class Cipher {
    private _key;
    /**
     * Creates a new Cipher.
     *
     * @param {string} [secretKey] - A Base64-encoded 32-byte long secret key. Will generate a new key if none is provided.
     */
    constructor(secretKey?: string);
    /**
     * Encrypts a message.
     * @param {string} message - The message to be encrypted.
     * @returns {string} The encrypted message.
     */
    encrypt(message: string): string;
    /**
     * Decrypts a message.
     *
     * @param {string} message - The Base64-encoded encrypted message to be decrypted.
     * @returns {string} The decrypted message.
     * @throws Throws an error if the decryption fails.
     */
    decrypt(message: string): string;
    /**
     * @returns {string} The cipher's secret key
     */
    readonly key: string;
}
