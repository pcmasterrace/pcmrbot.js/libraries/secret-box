"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tweetnacl_1 = require("tweetnacl");
const tweetnacl_util_1 = require("tweetnacl-util");
/**
 * Generates a random 24-byte long nonce.
 */
function generateNonce() {
    return tweetnacl_1.randomBytes(tweetnacl_1.secretbox.nonceLength);
}
exports.generateNonce = generateNonce;
/**
 * Generates a random 32-byte long secret key, encoded in Base64.
 */
function generateKey() {
    return tweetnacl_util_1.encodeBase64(tweetnacl_1.randomBytes(tweetnacl_1.secretbox.keyLength));
}
exports.generateKey = generateKey;
/**
 * Encrypts a raw string using the specified key.
 *
 * @param {string} raw - The string to encode.
 * @param {string} key - A Base64-encoded secret key.
 * @returns {string} The encrypted version of the `raw` string
 */
function encrypt(raw, key) {
    const keyUint8Array = tweetnacl_util_1.decodeBase64(key);
    const nonce = generateNonce();
    const messageUint8 = tweetnacl_util_1.decodeUTF8(raw);
    const box = tweetnacl_1.secretbox(messageUint8, nonce, keyUint8Array);
    const fullMessage = new Uint8Array(nonce.length + box.length);
    fullMessage.set(nonce);
    fullMessage.set(box, nonce.length);
    return tweetnacl_util_1.encodeBase64(fullMessage);
}
exports.encrypt = encrypt;
/**
 * Decrypts an encrypted string using the specified key.
 *
 * @param {string} messageWithNonce - The Base64-encoded encrypted message, with the first 24 bytes containing the nonce.
 * @param {string} key - A Base64-encoded secret key.
 * @returns {string} The decrypted string.
 * @throws Throws if the decryption fails.
 */
function decrypt(messageWithNonce, key) {
    const keyUint8Array = tweetnacl_util_1.decodeBase64(key);
    const messageWithNonceUint8 = tweetnacl_util_1.decodeBase64(messageWithNonce);
    const nonce = messageWithNonceUint8.slice(0, tweetnacl_1.secretbox.nonceLength);
    const message = messageWithNonceUint8.slice(tweetnacl_1.secretbox.nonceLength, messageWithNonce.length);
    const decrypted = tweetnacl_1.secretbox.open(message, nonce, keyUint8Array);
    if (!decrypted) {
        throw new Error("Could not decrypt message");
    }
    return tweetnacl_util_1.encodeUTF8(decrypted);
}
exports.decrypt = decrypt;
/**
 * This class allows for easy encoding and decoding of information using the same secret key.
 */
class Cipher {
    /**
     * Creates a new Cipher.
     *
     * @param {string} [secretKey] - A Base64-encoded 32-byte long secret key. Will generate a new key if none is provided.
     */
    constructor(secretKey = generateKey()) {
        this._key = secretKey;
    }
    /**
     * Encrypts a message.
     * @param {string} message - The message to be encrypted.
     * @returns {string} The encrypted message.
     */
    encrypt(message) {
        return encrypt(message, this._key);
    }
    /**
     * Decrypts a message.
     *
     * @param {string} message - The Base64-encoded encrypted message to be decrypted.
     * @returns {string} The decrypted message.
     * @throws Throws an error if the decryption fails.
     */
    decrypt(message) {
        return decrypt(message, this._key);
    }
    /**
     * @returns {string} The cipher's secret key
     */
    get key() {
        return this._key;
    }
    /**
     * Overrides toJSON to redact secret key when stringifying
     */
    toJSON() {
        let result = {};
        for (let x in this) {
            if (x !== "_key") {
                result[x] = this[x];
            } else {
                result[x] = "[REDACTED]"
            }
        }
        return result;
    }

}
exports.Cipher = Cipher;
